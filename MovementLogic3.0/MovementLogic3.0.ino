#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;
VL53L0X sensor2;
VL53L0X sensor3;
VL53L0X sensor4;

int E1 = 5;       //Motor 1 and 2 Speed Pins
int E2 = 2;
int E3 = 3;
int E4 = 4;

int M1 = 35;       //Motor 1 and 2 Direction Pins
int M2 = 32;
int M3 = 33;
int M4 = 34;

int S1 = 0;       //Motor 1 and 2 Speed variables
int S2 = 0;
int S3 = 0;
int s4 = 0;

bool D1 = 1;      //Motor 1 and 2 Direction variables
bool D2 = 2;

bool FORWARD = 1;
bool BACKWARD = 0;

int distance = 0;
int distance2 = 0;
int distance3 = 0;
int distance4 = 0;


void drive (char a, bool dir1, bool dir2) {
  analogWrite(E1, a);
  digitalWrite(M1, dir1);
  analogWrite(E2, a);
  digitalWrite(M2, dir2);
  analogWrite(E3, a);
  digitalWrite(M3, dir2);
  analogWrite(E4, a);
  digitalWrite(M4, dir1);
}




/*
  void turn (char a, char b,char c, char d, bool dir){
  analogWrite (E1, a);
  digitalWrite(M1, dir);
  analogWrite(E2, b);
  digitalWrite(M2, dir);
  analogWrite (E3, c);
  digitalWrite(M3, dir);
  analogWrite(E4, d);
  digitalWrite(M4, dir);
  }
*/

void setup() {

  pinMode(35, OUTPUT);
  pinMode(32, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);

  pinMode(18, OUTPUT);
  digitalWrite(18, LOW);
  pinMode(17, OUTPUT);
  digitalWrite(17, LOW);
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);
  pinMode(15, OUTPUT);
  digitalWrite(15, LOW);

  delay(500);

  Wire.begin();

  Serial.begin (9600);

  pinMode(15, INPUT);
  delay(150);
  Serial.println("00");
  sensor.init(true);
  Serial.println("01");
  delay(100);
  sensor.setAddress((uint8_t)22);
  Serial.println("02");

  pinMode(16, INPUT);
  delay(150);
  Serial.println("03");
  sensor2.init(true);
  Serial.println("04");
  delay(100);
  sensor2.setAddress((uint8_t)25);
  Serial.println("05");

  pinMode(17, INPUT);
  delay(150);
  Serial.println("06");
  sensor3.init(true);
  Serial.println("07");
  delay(100);
  sensor3.setAddress((uint8_t)28);
  Serial.println("08");

  pinMode(18, INPUT);
  delay(150);
  Serial.println("09");
  sensor4.init(true);
  Serial.println("10");
  delay(100);
  sensor4.setAddress((uint8_t)31);
  Serial.println("11");

  Serial.println("addresses set");

  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;


  for (byte i = 1; i < 120; i++)
  {

    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");

  delay(1000);


  sensor.setTimeout(500);
  sensor2.setTimeout(500);
  sensor3.setTimeout(500);
  sensor4.setTimeout(500);


  sensor.startContinuous();
  sensor2.startContinuous();
  sensor3.startContinuous();
  sensor4.startContinuous();

  //  distance = sensor.readRangeContinuousMillimeters(); // worked out from datasheet graph  Left Sensor
  //  distance2 = sensor2.readRangeContinuousMillimeters(); // worked out from datasheet graph  Front Sensor
  //  distance3 = sensor3.readRangeContinuousMillimeters(); // worked out from datasheet graph  Right Sensor
  //  distance4 = sensor4.readRangeContinuousMillimeters(); // worked out from datasheet graph  Back Sensor
  //
  //  delay(500); // slow down serial port
  //
  //  Serial.println(distance);
  //  Serial.println(distance2);
  //  Serial.println(distance3);
  //  Serial.println(distance4);

}

void loop() {

  distance = sensor.readRangeContinuousMillimeters(); // worked out from datasheet graph  Left Sensor
  distance2 = sensor2.readRangeContinuousMillimeters(); // worked out from datasheet graph  Back Sensor
  distance3 = sensor3.readRangeContinuousMillimeters(); // worked out from datasheet graph  Right Sensor
  distance4 = sensor4.readRangeContinuousMillimeters(); // worked out from datasheet graph  Front Sensor

  delay(500); // slow down serial port

  //  Serial.print("left= ");
  //  Serial.print(distance);
  //  Serial.println();
  //  Serial.print("back= ");
  //  Serial.print(distance2);
  //  Serial.println();
  //  Serial.print("right= ");
  //  Serial.print(distance3);
  //  Serial.println();
  //  Serial.print("front= ");
  //  Serial.print(distance4);
  //  Serial.println();


  //  if(Serial.available()){ // for testing wheels
  //    char val = Serial.read();
  //    switch(val){
  //
  //      case 'w':
  //        moveForward();
  //        break;
  //      case 's':
  //        moveBackward();
  //        break;
  //      case 'a':
  //        moveLeft();
  //        break;
  //      case 'd':
  //        moveRight();
  //        break;
  //      case 'r':
  //        stopMoving();
  //        break;
  //      default:
  //        break;
  //    }
  //  }


  if (Serial.available()) {
    byte val = Serial.read();
    byte firstBit = val / 100;      // ex 101 / 100 = 1 , 010 / 100 = 0
    byte secondBit = (val % 100) / 10; // ex 111 % 100 = 11 --> 11/10 = 1 , 101 % 100 = 1 --> 1/10 = 0
    byte thirdBit = val % 1;      // ex 101 % 1 = 1 or 100 % 1= 0

    beginning(firstBit);
    middle(secondBit);
    end(thirdBit);

  }


}

void moveLeft() {
  //Strafe left
  S1 = 100;
  D1 = 0;
  D2 = 1;
  drive(S1, D2, D1);
}

void moveRight() {
  //Strafe right
  S1 = 100;
  D1 = 0;
  D2 = 0;
  drive(S1, D2, D1);
}

void moveForward() {
  // move forward
  S2 = 100;
  D1 = 0;
  D2 = 0;
  drive(S2, D2, D1);
}

void moveBackward() {
  // move backward
  S2 = 100;
  D1 = 1;
  D2 = 1;
  drive(S2, D2, D1);
}

void stopMoving() {
  // stop
  S2 = 0;
  drive(S2, D2, D1);
}

void accelerate() {
  if (S2 <= 200) {
    S2 += 50;
  }
}

void decelerate() {
  if (S2 >= 50) {
    S2 -= 50;
  }
}

void changeDirection() {
  D2 = !D2;
  drive(S2, D2, D1);

}




void beginning(byte firstBit) {
  if (firstBit == 0) {
    while (distance > 100) {
      //Strafe left

      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveLeft();
      //S1 = 100;
      //D1=0;
      //D2 = 1;
      //drive(S1, D2, D1);
    }

    delay(250);

    while (distance <= distance3) {
      //Strafe right

      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveRight();
      //S1 = 100;
      //D1=1;
      //D2 = 0;
      //drive(S1, D2, D1);
    }
  }
  else {
    while (distance2 > 200) {
      //Strafe right
      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveRight();
      //S1 = 100;
      //D1=1;
      //D2 = 0;
      //drive(S1, D2, D1); //Strafe right
    }

    delay(250);

    while (distance >= distance3) {
      //Strafe left
      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveLeft();
      //S1 = 100;
      //D1=0;
      //D2 = 1;
      //drive(S1, D2, D1);
    }
  }

  delay(250);

  //Full Speed Fwd
  moveForward();

  delay(3000);

  //Stop
  stopMoving();

  // Current location - in front of the ramp
}

void middle(byte secondBit) {

  if (secondBit == 0) {

    while (distance > 50) {
      //Strafe left
      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveLeft();
      //S1 = 100;
      //D1=0;
      //D2 = 1;
      //drive(S1, D2, D1);
    }

    delay(250);

    while (distance2 > 40) {
      distance2 = sensor2.readRangeContinuousMillimeters();
      moveForward();
    }

    stopMoving();

    while (distance <= distance3) {
      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveRight();
    }

    stopMoving();
  }
  else {

    while (distance3 > 50) {
      //Strafe right
      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveRight();
      //S1 = 100;
      //D1=1;
      //D2 = 0;
      //drive(S1, D2, D1); //Strafe right
    }

    while (distance2 > 40) {
      distance2 = sensor2.readRangeContinuousMillimeters();
      moveForward();
    }

    stopMoving();

    while (distance >= distance3) {
      distance = sensor.readRangeContinuousMillimeters();
      distance3 = sensor3.readRangeContinuousMillimeters();
      moveLeft();
    }

    stopMoving();

  }
}

void end(byte thirdBit) {

  moveBackward();

  delay(1000);

  stopMoving();

  //pick up that booty

  moveBackward();

  delay(5000);

  if (thirdBit == 0) {
    moveLeft();
  }
  else {
    moveRight();
  }



}
